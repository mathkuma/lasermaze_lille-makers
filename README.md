# laserMaze - Labyrinthe Laser
labytinthe avec 4 lasers 

## Câblage
Pin Arduino  | Composant | Commentaire
---------|------------|------------
A0 |  LDR_1   |  photoresistor 1 + résistance pulldown 10 kOhms
A1 |  LDR_2   |  photoresistor 2 + résistance pulldown 10 kOhms
A2 |  LDR_3   |  photoresistor 3 + résistance pulldown 10 kOhms
A3 |  LDR_4   |  photoresistor 4 + résistance pulldown 10 kOhms
A4 |  POT_PIN   |   potentiometre
D2 |  LDR_1   |  diode laser 1
D3 |  LDR_2   |  diode laser 2
D4 |  LDR_3   |  diode laser 3
D5 |  LDR_4   |  diode laser 4
D6 |  LED_DATA_PIN | led RGB neoPixel
D7 |  ALARME_PIN | mosfet 2N7000 + alarme
D8 | MODE_PIN_0 | interrupteur 3 positions pour selection mode
D9 | MODE_PIN_1 | interrupteur 3 positions pour selection mode
D12 | BUT_CALIBRAGE |  bouton poussoir calibrage

## Schema 
![](./images/schema.png)