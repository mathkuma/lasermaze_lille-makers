/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <technoLARP@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return. Marcelin
 * ----------------------------------------------------------------------------
*/

/*
 * ----------------------------------------------------------------------------
 * Laser Maze LilleMakers
 * Marcelin Delcour, 01-2020
 * technoLARP (_@_) gmail (_._) com
 * ----------------------------------------------------------------------------
*/

/*
 * PINOUT
 * 
 * A0, A1, A2, A3 : capteur LDR 0 a 3
 * A4 :             potentiometre
 * 
 * D2, D3, D4, D5 : laser 0 a 3
 * D6 :             neopixel
 * D7 :             mosfet 2n7000 ./check_process.sh
 * D8,D9 :          interrupteur 3 pos pour selection mode
 * D12 :            bouton poussoir calibrate
 * 
 * dispo
 * A5,D10, D11
 * 
*/

// LASER & LDR
#define NB_LASER 4

int laser[NB_LASER]={2,3,4,5};
int capteur[NB_LASER]={A0,A1,A2,A3};

int laserIndex=0;
int laserStatut[NB_LASER] = {1,1,1,1};
int delayOnOff = 25;

// tableau
int capteurValue[NB_LASER];
int capteurRefValue[NB_LASER];
int capteurAlarme[NB_LASER];

boolean alarmeActive;
int sensibilite = 50;       // sensibilité des capteurs

// INPUT / OUTPUT
int buttonCalibrate = 12;

int mode0 = 8; 
int mode1 = 9; 

// LASER MODE
enum {
  LASER_CHENILLARD = 1, 
  LASER_ALEATOIRE = 2,
  LASER_1SUR2 = 3,
  LASER_CONTINU = 4
  };
byte modeLaser = LASER_CONTINU;
byte modeLaserPrev = LASER_CONTINU;

int potar = A4;
int alarm = 7;

// TIME REFERENCE
unsigned long int ul_PreviousMillis;
unsigned long int ul_CurrentMillis;
unsigned long int ul_Interval;


// NEOPIXEL
#include <FastLED.h>  // https://github.com/FastLED/FastLED

#define NUM_LEDS NB_LASER
#define DATA_PIN 6

// Define the array of leds
CRGB leds[NUM_LEDS];



void setup() 
{
  Serial.begin(115200);
  
  // INIT IO
  for (int i=0;i<NB_LASER;i++)
  {
    pinMode(laser[i],OUTPUT);
    digitalWrite(laser[i],HIGH);

    pinMode(capteur[i],INPUT);
    digitalWrite(laser[i],LOW);
  }

  pinMode(buttonCalibrate,INPUT);
  digitalWrite(buttonCalibrate, HIGH);

  pinMode(mode0,INPUT);
  digitalWrite(mode0, HIGH);

  pinMode(mode1,INPUT);
  digitalWrite(mode1, HIGH);

  pinMode(potar,INPUT);
  digitalWrite(potar, LOW);

  pinMode(alarm,OUTPUT);
  digitalWrite(alarm, LOW);

  randomSeed(analogRead(A5));

  // NEOPIXEL
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  FastLED.setBrightness(50);

  // startup animation
  FastLED.clear();
  
  int delayLed = 50;
  
  for (int k=0;k<6;k++)
  {
    for (int i=0;i<NUM_LEDS;i++)
    {
      if (k%2 == 0)
      {
        leds[i] = CRGB::Yellow;
        FastLED.show();
        delay(delayLed);
        leds[i] = CRGB::Black;
      }
      else
      {
        leds[NUM_LEDS-1-i] = CRGB::Yellow;
        FastLED.show();
        delay(delayLed);
        leds[NUM_LEDS-1-i] = CRGB::Black;
      }
    }
  }
  FastLED.clear();
  FastLED.show();
    
  // LASER
  laserOn();
  delay(500);
  calibrate();
  delay(1000);
  
  // START
  Serial.println("START !!");
}





void loop() 
{
  /* --- gestion bouton recalibre --- */
  if (digitalRead(buttonCalibrate)==LOW)
  {    
    Serial.println("Calibrate ");
    while(digitalRead(buttonCalibrate)==LOW)
    {
      delay(50);
    }

    // on active tous les lasers
    laserOn();
    delay(1000);
    // on calibre les capteurs
    calibrate();
    delay(1000);
  }
  

  /* --- gestion capteurs --- */
  // on met a jour les mesures des capteurs
  majCapteurValue();

  /* --- gestion coupure lasers --- */
  for (int i=0;i<NB_LASER;i++)
  {
    // test si l emetteur est actif  
    if (laserStatut[i] == 1)
    {
      // test si le laser est coupé
      if ( capteurValue[i] < ((capteurRefValue[i]*0.6) - sensibilite)   &&   capteurAlarme[i]==0 )
      {
        // laser coupé, desactiver les autres check capteur
        capteurAlarme[i]=1;        
           
        Serial.print("COUPURE LASER n° ");
        Serial.println(i+1); 

        // ALARME !!
        alarme();

        // Blink led
        digitalWrite(alarm, HIGH);
        blinkLed(i, CRGB::Red, 6);
        leds[i]=CRGB::Red;
        digitalWrite(alarm, LOW);
      }
      else
      {
        if ( capteurValue[i] >= (capteurRefValue[i] - sensibilite))
        {
          // le laser n'est plus coupé
          capteurAlarme[i]=0;
        }
      }
    }
  }

  /* --- maj mode laser --- */
  int b0=digitalRead(mode0);
  int b1=digitalRead(mode1);

  modeLaser=0;
  bitWrite(modeLaser, 0, b0);
  bitWrite(modeLaser, 1, b1);
  
  /* --- gestion changement mode --- */
  ul_CurrentMillis = millis();
  if(ul_CurrentMillis - ul_PreviousMillis > ul_Interval)
  {
    ul_PreviousMillis = ul_CurrentMillis;
    ul_Interval=map(analogRead(potar),0,1024,0,9)*250;
    
    /*
    Serial.print(b0);
    Serial.print(" ");
    Serial.print(b1);
    Serial.print(" ");
    Serial.println(modeLaser);
    Serial.print("interval:  ");
    Serial.println(ul_Interval);
    */

    // bypass si ul_Interval == 0
    // on passe le délai à 2000 et modeLaser à LASER_CONTINU
    if (ul_Interval==0)
    {
      modeLaser=LASER_CONTINU; //mode laser en continu
      ul_Interval=2000;
    }

    /* --- afficher les valeurs des capteurs --- */
    for (int i=0;i<NB_LASER;i++)
    {
      Serial.print(capteurValue[i]);
      Serial.print("  ");
      
    }
    Serial.println("");
    
    /* --- afficher changement mode laser --- */
    if (modeLaser != modeLaserPrev)
    {
      modeLaserPrev = modeLaser;
      Serial.print("changement laser mode pour: ");

      switch (modeLaser)
      {
        case LASER_1SUR2:
          // 1 sur 2
          Serial.println("0: 1sur2");
        break;

        case LASER_ALEATOIRE:
          // aleatoire
          Serial.println("1: aleatoire");
        break;
    
        case LASER_CHENILLARD:
          // chenillard
          Serial.println("2: chenillard");
        break;        
        
        case LASER_CONTINU:
        default:
          // continu
          Serial.println("4: continu");          
        break; 
        }
        Serial.print("délai: ");
        Serial.println(ul_Interval);
    }
      
    /* --- gestion du mode laser --- */
    switch (modeLaser)
      {
      case LASER_1SUR2:
      // 1 sur 2
        modeLaser1sur2();
      break;
      
      case LASER_ALEATOIRE:
        // aleatoire
        modeLaserAleatoire();
      break;
  
      case LASER_CHENILLARD:
        // chenillard
        modeLaserChenillard();
      break;
      
      case LASER_CONTINU:
      default:
        // continu
        modeLaserContinu();
      break; 
      }
  }
}

void majCapteurValue()
{
  for (int i=0;i<NB_LASER;i++)
  {
    capteurValue[i]=0;
    
    // lire la valeur 5 fois et la stocker
    for (int j=0;j<5;j++)
    {
      capteurValue[i] += analogRead(capteur[i]);    // 5 fois
      //delay(1);
    }
    // calcul moyenne  
    capteurValue[i] /= 5;
  }
}

void calibrate() 
{
  Serial.println("CALIBRAGE EN COURS");

  for (int i=0;i<NB_LASER;i++)
  {
    capteurRefValue[i] = 0;

    // LED
    leds[i] = CRGB::Blue;
    FastLED.show();

    // lire la valeur 5 fois et la stocker
    for (int j=0;j<5;j++)
    {
      capteurRefValue[i] += analogRead(capteur[i]);  // 5
      delay(50);    
    }
    capteurRefValue[i] /= 5;      // moyenne
    capteurValue[i] = capteurRefValue[i];

    Serial.print("capteur ");
    Serial.print(i+1);
    Serial.print(" = ");
    Serial.println(capteurRefValue[i]);
  }
    
  Serial.println("CALIBRAGE TERMINE");
}

void modeLaser1sur2()
{
    // 1 sur 2
    for (int i=0;i<NB_LASER;i++)
    {
      laserStatut[i]=1;
      laserOn(i);
    }

    laserIndex=(laserIndex+1)%NB_LASER;

    laserOn(laserIndex);
    laserOn((laserIndex+2)%NB_LASER);

    laserStatut[(laserIndex+1)%NB_LASER]=0;
    laserStatut[(laserIndex+3)%NB_LASER]=0;

    laserOff((laserIndex+1)%NB_LASER);
    laserOff((laserIndex+3)%NB_LASER);

    //LED
    leds[laserIndex] = CRGB::Green;
    leds[(laserIndex+2)%NB_LASER] = CRGB::Green;

    leds[(laserIndex+1)%NB_LASER] = CRGB::Black;
    leds[(laserIndex+3)%NB_LASER] = CRGB::Black;
    FastLED.show();
}

void modeLaserAleatoire()
{
  // aleatoire
  for (int i=0;i<NB_LASER;i++)
  {
    int aleat=random(0,2)%2;

    if (aleat==0)
    {
      laserStatut[i]=0;
      laserOff(i);
      leds[i] = CRGB::Black;
    }
    else
    {
      laserStatut[i]=1;
      laserOn(i);
      leds[i] = CRGB::Green;
    }
  }

  //LED
  FastLED.show();
}

void modeLaserChenillard()
{
  // chenillard
  for (int i=0;i<NB_LASER;i++)
  {
    laserStatut[i]=1;
  }

  laserIndex=(laserIndex+1)%NB_LASER;
  laserStatut[laserIndex]=0;

  for (int i=0;i<NB_LASER;i++)
  {
    if (laserStatut[i]==1)
    {
      laserOn(i);
      leds[i] = CRGB::Green;
    }
    else
    {
      laserOff(i);
      leds[i] = CRGB::Black;
    }
  }

  //LED
  FastLED.show();
}

void modeLaserContinu()
{
  // continu
  for (int i=0;i<NB_LASER;i++)
  {
    laserStatut[i]=1;
    laserOn(i);
    leds[i] = CRGB::Green;
  }

  //LED
  FastLED.show();
}

void laserOn() 
{
  for (int i=0;i<NB_LASER;i++)
  {
    laserOn(i);
  }
    
  Serial.println("send laser ON");
}

void laserOff() 
{
  for (int i=0;i<NB_LASER;i++)
  {
    laserOff(i);
  }

  Serial.println("send laser OFF");
}

void laserOn(int laserNum) 
{
  digitalWrite(laser[laserNum],LOW);
  delay(5);
}

void laserOff(int laserNum) 
{
  digitalWrite(laser[laserNum],HIGH);
  delay(5);
}

void alarme()
{
  
}

// blink the led X times
void blinkLed(int ledID, CRGB color, int count)
{
  for (int i=0;i<count;i++)
  {
    leds[ledID] = color;
    FastLED.show();
    delay(100);
    leds[ledID] = CRGB::Black;
    FastLED.show();
    delay(100);
  } 
}
